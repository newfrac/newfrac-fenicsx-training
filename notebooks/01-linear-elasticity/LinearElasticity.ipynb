{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "included-constitution",
   "metadata": {},
   "source": [
    "# Linear Elasticity\n",
    "\n",
    "*Authors: Jack Hale, (Univ. Luxembourg), Laura De Lorenzis (ETH Zürich) and Corrado Maurini (corrado.maurini@sorbonne-universite.fr)*\n",
    "\n",
    "This notebook serves as a tutorial to solve a problem of linear elasticity using DOLFINx.\n",
    "\n",
    "You can find a tutorial and useful resources for DOLFINx at the following links\n",
    "\n",
    "- https://docs.fenicsproject.org/\n",
    "- https://github.com/FEniCS/dolfinx/\n",
    "- https://jorgensd.github.io/dolfinx-tutorial/, see https://jorgensd.github.io/dolfinx-tutorial/chapter2/linearelasticity.html for linear elasticity\n",
    "\n",
    "We consider an elastic slab $\\Omega$ with a straight crack $\\Gamma$ subject to a mode-I loading by an applied traction force $f$, see figure. \n",
    "\n",
    "Using the symmetry, we will consider only half of the domain in the computation.\n",
    "\n",
    "![title](./domain.png)\n",
    "\n",
    "We solve the problem of linear elasticity with the finite element method, implemented using DOLFINx.\n",
    "\n",
    "FEniCSX is advanced library that allows for efficient parallel computation. For the sake of simplicity, we assume here to work on a single processor and will not use MPI-related commands. Using DOLFINx with MPI will be covered in the afternoon session."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "progressive-invite",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.path.append(\"../python\")\n",
    "\n",
    "# Import required libraries\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "import dolfinx\n",
    "import ufl\n",
    "\n",
    "from mpi4py import MPI\n",
    "from petsc4py import PETSc\n",
    "\n",
    "from utils import project\n",
    "from meshes import generate_mesh_with_crack\n",
    "\n",
    "plt.rcParams[\"figure.figsize\"] = (40,6)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "handled-making",
   "metadata": {},
   "source": [
    "Let us generate a mesh using gmsh (http://gmsh.info/). \n",
    "\n",
    "The function to generate the mesh is reported in the external file `meshes.py`. \n",
    "The mesh is refined around the crack tip."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "superior-exhibition",
   "metadata": {},
   "outputs": [],
   "source": [
    "Lx = 1.\n",
    "Ly = .5\n",
    "Lcrack = 0.3\n",
    "lc =.2\n",
    "dist_min = .1\n",
    "dist_max = .3\n",
    "mesh = generate_mesh_with_crack(Lcrack=Lcrack,\n",
    "                 Lx=Lx,\n",
    "                 Ly=Ly,\n",
    "                 lc=lc, # characteristic length of the mesh\n",
    "                 refinement_ratio=20, # how much to refine at the tip zone\n",
    "                 dist_min=dist_min, # radius of tip zone\n",
    "                 dist_max=dist_max # radius of the transition zone \n",
    "                 )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "magnetic-slovak",
   "metadata": {},
   "source": [
    "To plot the mesh we use `pyvista` see:\n",
    "- https://jorgensd.github.io/dolfinx-tutorial/chapter3/component_bc.html\n",
    "- https://docs.fenicsproject.org/dolfinx/main/python/demos/pyvista/demo_pyvista.py.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "peripheral-stretch",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyvista\n",
    "import dolfinx.plot\n",
    "\n",
    "if pyvista.OFF_SCREEN:\n",
    "    from pyvista.utilities.xvfb import start_xvfb\n",
    "    start_xvfb(wait=0.1)\n",
    "\n",
    "transparent = False\n",
    "figsize = 800\n",
    "pyvista.rcParams[\"background\"] = [0.5, 0.5, 0.5]\n",
    "\n",
    "topology, cell_types = dolfinx.plot.create_vtk_topology(mesh, mesh.topology.dim)\n",
    "\n",
    "grid = pyvista.UnstructuredGrid(topology, cell_types, mesh.geometry.x)\n",
    "\n",
    "from pyvista.utilities.xvfb import start_xvfb\n",
    "start_xvfb(wait=0.5)\n",
    "plotter = pyvista.Plotter()\n",
    "plotter.add_mesh(grid, show_edges=True, show_scalar_bar=True)\n",
    "plotter.view_xy()\n",
    "if not pyvista.OFF_SCREEN:\n",
    "    plotter.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "wrapped-yellow",
   "metadata": {},
   "source": [
    "## Finite element function space\n",
    "\n",
    "We use here linear Lagrange triangle elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "owned-alarm",
   "metadata": {},
   "outputs": [],
   "source": [
    "element = ufl.VectorElement('Lagrange',mesh.ufl_cell(),degree=1,dim=2)\n",
    "V = dolfinx.FunctionSpace(mesh, element)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "returning-cricket",
   "metadata": {},
   "source": [
    "## Dirichlet boundary conditions\n",
    "\n",
    "We define below the functions to impose the Dirichlet boundary conditions.\n",
    "\n",
    "In our case we want to \n",
    "- block the vertical component $u_1$ of the displacement on the part of the bottom boundary without crack\n",
    "- block the horizontal component $u_0$ on the right boundary\n",
    "\n",
    "We first get the dofs we need to block through specific dolfinx functions.\n",
    "\n",
    "The function `locate_dofs_geometrical` takes the dofs of the function space `V.sub(comp)` associated to the component `comp`. The syntax is not intuitive here. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "confirmed-memphis",
   "metadata": {},
   "outputs": [],
   "source": [
    "def bottom_no_crack(x):\n",
    "\n",
    "    return np.logical_and(np.isclose(x[1], 0.0), \n",
    "                          x[0] > Lcrack)\n",
    "\n",
    "def right(x):\n",
    "    return np.isclose(x[0], Lx)\n",
    "\n",
    "V_x = V.sub(0).collapse()\n",
    "V_y = V.sub(1).collapse()\n",
    "\n",
    "blocked_dofs_bottom = dolfinx.fem.locate_dofs_geometrical((V.sub(1), V_y), bottom_no_crack)\n",
    "blocked_dofs_right = dolfinx.fem.locate_dofs_geometrical((V.sub(0), V_x), right)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "wooden-scope",
   "metadata": {},
   "source": [
    "The following lines define the `dolfinx.DirichletBC` objects. We impose a zero displacement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "unexpected-poster",
   "metadata": {},
   "outputs": [],
   "source": [
    "zero_uy = dolfinx.Function(V_y)\n",
    "with zero_uy.vector.localForm() as bc_local:\n",
    "    bc_local.set(0.0)\n",
    "\n",
    "zero_ux = dolfinx.Function(V_x)\n",
    "with zero_ux.vector.localForm() as bc_local:\n",
    "    bc_local.set(0.0)\n",
    "      \n",
    "bc0 = dolfinx.DirichletBC(zero_uy, blocked_dofs_bottom, V.sub(1))\n",
    "bc1 = dolfinx.DirichletBC(zero_ux, blocked_dofs_right, V.sub(0))\n",
    "bcs = [bc0, bc1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "lovely-parker",
   "metadata": {},
   "source": [
    "## Define the bulk and surface mesures\n",
    "The bulk (`dx`) and surface (`ds`) measures are used by `ufl`to write variational form with integral over the domain or the boundary, respectively. In this example the surface measure `ds` includes tags to specify Neumann bcs: `ds(1)` will mean the integral on the top boundary. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "artificial-vinyl",
   "metadata": {},
   "outputs": [],
   "source": [
    "dx = ufl.Measure(\"dx\",domain=mesh)\n",
    "top_facets = dolfinx.mesh.locate_entities_boundary(mesh, 1, lambda x : np.isclose(x[1], Ly))\n",
    "mt = dolfinx.mesh.MeshTags(mesh, 1, top_facets, 1)\n",
    "ds = ufl.Measure(\"ds\", subdomain_data=mt)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "relevant-matrix",
   "metadata": {},
   "source": [
    "In Python, you can get help on the different functions with the folling syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "supported-westminster",
   "metadata": {},
   "outputs": [],
   "source": [
    "help(dolfinx.mesh.MeshTags)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sitting-sleep",
   "metadata": {},
   "source": [
    "## Define the variational problem \n",
    "\n",
    "We specify the problem to solve though the weak formulation written in the [ufl](https://fenics.readthedocs.io/projects/ufl/en/latest/) syntax by giving the bilinear $a(u,v)$ and linear forms $L(v)$ in \n",
    "the weak formulation: find the *trial function* $u$ such that for all *test function* $v$\n",
    "$$a(u,v)=L(v)$$ with \n",
    "\n",
    "$$a(u,v)=\\int_{\\Omega\\setminus\\Gamma}\\sigma(\\varepsilon(u))\\cdot \\varepsilon(v)\\,\\mathrm{d}x, \n",
    "\\quad L(v)=\\int_\\Omega b\\cdot v \\,\\mathrm{d}x + \\int_{\\partial_N\\Omega} f\\cdot v \\,\\mathrm{d}s $$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rural-childhood",
   "metadata": {},
   "source": [
    "ufl.inner(sigma(eps(u)), eps(v)) is an expression\n",
    "ufl.inner(sigma(eps(u)), eps(v)) * dx is a form"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "documented-owner",
   "metadata": {},
   "outputs": [],
   "source": [
    "u = ufl.TrialFunction(V)\n",
    "v = ufl.TestFunction(V)\n",
    "\n",
    "E = 1. \n",
    "nu = 0.3 \n",
    "mu = E / (2.0 * (1.0 + nu))\n",
    "lmbda = E * nu / ((1.0 + nu) * (1.0 - 2.0 * nu))\n",
    "# this is for plane-stress\n",
    "lmbda = 2*mu*lmbda/(lmbda+2*mu)\n",
    "\n",
    "def eps(u):\n",
    "    \"\"\"Strain\"\"\"\n",
    "    return ufl.sym(ufl.grad(u))\n",
    "\n",
    "def sigma(eps):\n",
    "    \"\"\"Stress\"\"\"\n",
    "    return 2.0 * mu * eps + lmbda * ufl.tr(eps) * ufl.Identity(2)\n",
    "\n",
    "def a(u,v):\n",
    "    \"\"\"The bilinear form of the weak formulation\"\"\"\n",
    "    k = 1.e+6\n",
    "    return ufl.inner(sigma(eps(u)), eps(v)) * dx \n",
    "\n",
    "def L(v): \n",
    "    \"\"\"The linear form of the weak formulation\"\"\"\n",
    "    # Volume force\n",
    "    b = dolfinx.Constant(mesh,ufl.as_vector((0,0)))\n",
    "\n",
    "    # Surface force on the top\n",
    "    f = dolfinx.Constant(mesh,ufl.as_vector((0,0.1)))\n",
    "    return ufl.dot(b, v) * dx + ufl.dot(f, v) * ds(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "crude-tennis",
   "metadata": {},
   "source": [
    "## Define the linear problem and solve\n",
    "We solve the problem using a direct solver. The class `dolfinx.fem.LinearProblem` assemble the stiffness matrix and load vector, apply the boundary conditions, and solve the linear system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "established-latitude",
   "metadata": {},
   "outputs": [],
   "source": [
    "problem = dolfinx.fem.LinearProblem(a(u,v), L(v), bcs=bcs, \n",
    "                                    petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"})\n",
    "uh = problem.solve()\n",
    "uh.name = \"displacement\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "subjective-preparation",
   "metadata": {},
   "source": [
    "## Postprocessing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "moved-world",
   "metadata": {},
   "source": [
    "We can easily calculate the potential energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "narrative-connecticut",
   "metadata": {},
   "outputs": [],
   "source": [
    "energy = dolfinx.fem.assemble_scalar(0.5 * a(uh, uh) - L(uh))\n",
    "print(f\"The potential energy is {energy:2.3e}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "outside-houston",
   "metadata": {},
   "source": [
    "We can save the results to a file, that we can open with `paraview` (https://www.paraview.org/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "announced-example",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "Path(\"output\").mkdir(parents=True, exist_ok=True)    \n",
    "with dolfinx.io.XDMFFile(MPI.COMM_WORLD, \"output/elasticity-demo.xdmf\", \"w\") as file:\n",
    "        file.write_mesh(uh.function_space.mesh)\n",
    "        file.write_function(uh)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "incorporated-brain",
   "metadata": {},
   "source": [
    "Let us plot the solution using `pyvista`, see\n",
    "- https://jorgensd.github.io/dolfinx-tutorial/chapter3/component_bc.html\n",
    "- https://docs.fenicsproject.org/dolfinx/master/python/demos/pyvista/demo_pyvista.py.html£"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "laughing-plain",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create plotter and pyvista grid\n",
    "p = pyvista.Plotter(title=\"Deflection\", window_size=[800, 800])\n",
    "topology, cell_types = dolfinx.plot.create_vtk_topology(mesh, mesh.topology.dim)\n",
    "grid = pyvista.UnstructuredGrid(topology, cell_types, mesh.geometry.x)\n",
    "\n",
    "# Attach vector values to grid and warp grid by vector\n",
    "vals_2D = uh.compute_point_values().real \n",
    "vals = np.zeros((vals_2D.shape[0], 3))\n",
    "vals[:,:2] = vals_2D\n",
    "grid[\"u\"] = vals\n",
    "actor_0 = p.add_mesh(grid, style=\"wireframe\", color=\"k\")\n",
    "warped = grid.warp_by_vector(\"u\", factor=1.5)\n",
    "actor_1 = p.add_mesh(warped, show_edges=True)\n",
    "p.view_xy()\n",
    "if not pyvista.OFF_SCREEN:\n",
    "   p.show()\n",
    "fig_array = p.screenshot(f\"output/displacement.png\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "nervous-slide",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}