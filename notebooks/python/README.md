# Utility functions

We collect below the utility functions we used in several places of the notebooks

## Mesh generation
- [python/meshes.py](https://gitlab.com/newfrac/newfrac-fenicsx-training/-/blob/main/notebooks/python/meshes.py)

## Nonlinear problems
- [python/nonlinear_pde_problem.py](https://gitlab.com/newfrac/newfrac-fenicsx-training/-/blob/main/notebooks/python/nonlinear_pde_problem.py)
- [python/snes_problem.py](https://gitlab.com/newfrac/newfrac-fenicsx-training/-/blob/main/notebooks/python/snes_problem.py)

## Others
- [python/utils.py](https://gitlab.com/newfrac/newfrac-fenicsx-training/-/blob/main/notebooks/python/utils.py)